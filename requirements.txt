mkdocs==1.1.2
Pygments==2.7.1
markdown==3.3.1
pymdown-extensions==8.0.1
mkdocs-material==6.0.2
mkdocs-material-extensions==1.0.1
mkdocs-minify-plugin==0.3.0
mkdocs-git-revision-date-localized-plugin==0.7.2
mkdocs-awesome-pages-plugin==2.4.0
requests==2.24.0

